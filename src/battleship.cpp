#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>

// Structs to hold arrays
struct coordWrap {
    int coords[3] = {};
};
struct attack {
    int coords[2] = {};
};
struct boardWrap {
    int board[10][10]={};
};
struct ship // struct variables to create ships
{
    std::string type;
    int hp;
};
const int hit = 20;
const int miss = 30;
const int empty = 0;
const int board_limit = 9;
const int board_start = 0;

int player_turns = 0; // keep count of player turns
int cpu_turns = 0; // keep count of cpu turns
int player_hits = 0; // keep count on player scoring hits
int cpu_hits = 0; // keep count on cpu scoring hits
int player_destroyed = 0; // keep count on how many ships player destoyed
int cpu_destroyed = 0; // keep count on how many ships cpu destroyed

int diagonals_searched = 0; //Lets the AI jump between diagonals, optimizing the search

bool isGameOver = true;
bool hit_horizontal = false; //Telling the AI it has hit a horizontal or vertically placeed ship
int hit_vertical = 0;

bool wrong_directionx = false; //Variables telling AI it looked in the right or wrong direction
int wrong_directiony = 0;

int last_vertical; //Defines the last moves for the AI
int last_horizontal;

int next_vertical = 0; //Defines the next move for the AI
int next_horizontal = 0;

int AI_diagonal_y = 0; //Variables to get back into main diagonal algorithm after destroying a ship
int AI_diagonal_x = 0;

bool found_ship = false; //Tells AI if it hit a ship or not
std::string whatToDraw(int num, int boardMode);

void drawBoard(int board[10][10], int boardMode);
void cpu_hp_check(const std::vector<ship> &cpu_ships);
void player_hp_check(const std::vector<ship> &player_ships);
void cpu_ship_hit(std::vector<ship> &cpu_ships, int ship_hit);
void player_ship_hit(std::vector<ship> &player_ships, int ship_hit);
void player_attack(std::vector<ship> &cpu_ships, attack coordinates);
void AI_attack(std::vector<ship> &player_ships);
std::vector<ship> ship_creation();
struct boardWrap placementRandom();
struct boardWrap placementManual();
void game_statistics();

void calculateNextMove();
void calculate_diagonal();
void calculate_homing();
void AI_reset();

struct attack Attack();
struct boardWrap player_board;
struct boardWrap AIBoard;

struct coordWrap inputToCoords(std::string userInput);

int main(){
    AIBoard = placementRandom();
    char placement_option;
    std::cout<<"Choose how you want to create your ships:"<<std::endl;
    std::cout<<"Enter 1 for random placements"<<std::endl;
    std::cout<<"Enter 2 for manual placements"<<std::endl;
    while (true)
    {
        std::cin>>placement_option;
        if (placement_option == '1')
        {
            player_board = placementRandom();
            break;
        }else if (placement_option == '2')
        {
            player_board = placementManual();
            break;
        }
        std::cout<<"Invalid input! Try again"<<std::endl;
    }
    
    
    std::vector<ship> player_ships; // create player ships
    player_ships = ship_creation();
    std::vector<ship> cpu_ships; // create cpu ships
    cpu_ships = ship_creation();
    std::string coordinate_input;
    std::cout<<"Please enter 0 for normal mode or 1 for cheat mode: ";
    
    int mode;
    std::cin>>mode;
    if (mode == 0)
    {
        mode = 1;
    }else if (mode == 1)
    {
        mode = -1;
    }
    
    while (isGameOver)
    {
        
        //std::cin.ignore();
        attack attack_coords = Attack();
        if (attack_coords.coords[0] == -1 || attack_coords.coords[1] == -1)
        {
            continue;
        }
        std::system("clear");
        player_attack(cpu_ships,attack_coords);
        AI_attack(player_ships);
        
        std::cout<<"Your board: "<<std::endl;
        drawBoard(player_board.board,0);
        std::cout<<std::endl;
        std::cout<<"Computer's board: "<<std::endl;
        drawBoard(AIBoard.board,mode);
        std::cout<<std::endl;
        
        calculateNextMove();
    }

    player_hp_check(player_ships); // call player hp check
    cpu_hp_check(cpu_ships); // call cpu hp check

    return 0;
}

std::vector<ship> ship_creation(){

    std::vector<ship> ships;
    ships.push_back({"dummy", 0});
    ships.push_back({"Carrier", 5});
    ships.push_back({"Battleship",4});
    ships.push_back({"Destroyer", 3});
    ships.push_back({"Submarine", 3});
    ships.push_back({"Patrol Boat", 2});

    return ships;
}

void player_attack(std::vector<ship> &cpu_ships, attack coordinates){
    //Marks the position as miss if it's empty
    if(AIBoard.board[coordinates.coords[1]][coordinates.coords[0]] == 0){
        AIBoard.board[coordinates.coords[1]][coordinates.coords[0]] = miss;
    }else{
        //Changing values when hitting a ship
        player_hits++;
        cpu_ship_hit(cpu_ships, AIBoard.board[coordinates.coords[1]][coordinates.coords[0]]);
        AIBoard.board[coordinates.coords[1]][coordinates.coords[0]] = hit;
    }
    player_turns++;
}

//Shoot the previously calculated target
void AI_attack(std::vector<ship> &player_ships){
    //Marks the position as miss if it's empty
    if(player_board.board[next_vertical][next_horizontal] == 0){
        player_board.board[next_vertical][next_horizontal] = miss;                 
    }else if(player_board.board[next_vertical][next_horizontal] > 0 && player_board.board[next_vertical][next_horizontal] < 6){
        //Changing values when hitting a ship
        cpu_hits++;
        found_ship = true;
        player_ship_hit(player_ships, player_board.board[next_vertical][next_horizontal]);
        player_board.board[next_vertical][next_horizontal] = hit;
        
    }
    cpu_turns++;
}

void player_hp_check(const std::vector<ship> &player_ships) // check if players total hp is 0
{
    int hpsum_player = player_ships[1].hp + player_ships[2].hp + player_ships[3].hp + player_ships[4].hp + player_ships[5].hp;
    if (hpsum_player == 0)
    {
        std::cout << "Player lost the game" << std::endl;;
        isGameOver = false;
        game_statistics();
    }
}

void cpu_hp_check(const std::vector<ship> &cpu_ships) // check if CPU total hp is 0
{
    int hpsum_cpu = cpu_ships[1].hp + cpu_ships[2].hp + cpu_ships[3].hp + cpu_ships[4].hp + cpu_ships[5].hp;
    if (hpsum_cpu == 0)
    {
        std::cout << "CPU lost the game" << std::endl;
        isGameOver = false;
        game_statistics();
    }
}

void cpu_ship_hit(std::vector<ship> &cpu_ships, int ship_hit) // substracts hit ship hp and check if destroyed
{
    cpu_ships[ship_hit].hp--; // int ship_hit need to be declared somewhere
    if (cpu_ships[ship_hit].hp == 0)
    {
        player_destroyed++;
        std::cout << "CPU " << cpu_ships[ship_hit].type << " destroyed"<<std::endl;

        return cpu_hp_check(cpu_ships); // call function to check if any ships are alive
    }
}

void player_ship_hit(std::vector<ship> &player_ships, int ship_hit) // substracts hit ship hp and check if destroyed
{
    player_ships[ship_hit].hp--; // int ship_hit need to be declared somewhere
    if (player_ships[ship_hit].hp == 0)
    {
        cpu_destroyed++;
        std::cout << "Player" << player_ships[ship_hit].type << " destroyed"<<std::endl;
        AI_reset();
        return player_hp_check(player_ships); // call function to check if any ships are alive
    }
}

void drawBoard(int board[10][10], int boardMode){

    std::cout<<"  A B C D E F G H I J\n";
    for(int i=0;i<10;i++){
        if(i == 9){
            std::cout<<i<<" ";
        }else{
            std::cout<<i<<" ";
        }
        
        for(int j=0;j<10;j++){
            std::cout<< whatToDraw(board[i][j], boardMode);

        }
        std::cout<<"\n";
    }
}

std::string whatToDraw(int num, int boardMode){
    // boardMode 0 draws standard player board
    // boardMode 1 draws omits ship symbols for ai board
    // boardMode -1 is used for cheat mode, displays next ai shot
    
    if(num == 0){
        return "  "; // empty space symbol
    }else if(num == 20){
        return "X "; // hit marker
    }else if(num == 30){
        return "O "; //miss marker
    }if(num == -1){
        return ". "; // buffer zone marker for manual placement
    }
    if(boardMode == 0){ // 
        if(num == 1){
            return "C "; // carrier symbol
        }else if(num == 2){
            return "B "; // battleship symbol
        }else if(num == 3){
            return "D "; // destroyers symbol
        }else if(num == 4){
            return "S "; // submarine symbol
        }else if(num == 5){
            return "P "; // patrol boat symbol
        }
    }else if(boardMode == 1){
        return "  ";
    }
    return "Z ";//we shouldn't get here

}

struct boardWrap placementManual(){

    struct boardWrap boardInHere;
    for(int i = 0; i<10; i++){
        for(int j = 0; j<10; j++){
            boardInHere.board[i][j]=0;
        }
    }
    

    //std::system("clear");
    //drawBoard(boardInHere.board, 0);
    int shipsPlaced = 1;
    std::string input;

    // Loop continues until 5 ships successfully placed
    LOOP:do{

        //std::system("clear");
        drawBoard(boardInHere.board, 0);

        int shipLength;
        if(shipsPlaced == 1){
            shipLength = 5;
        }else if(shipsPlaced == 2){
            shipLength = 4;
        }else if(shipsPlaced == 4 || shipsPlaced == 3){
            shipLength = 3;
        }else if(shipsPlaced == 5){
            shipLength = 2;
        }



        std::cout<<"Input a ship origin coordinate and direction to place a ship\n";
        std::cout<<"Format: letter+number+direction d down from origin, r right, example a5d\n";
        std::cout<<"currently placing "<<shipLength<<" long ship\n";

        
        std::cin>>input;
        struct coordWrap testcoord = inputToCoords(input);
        int x = testcoord.coords[0];
        int y = testcoord.coords[1];
        int dir = testcoord.coords[2];
        //check input correctness, return to start of loop if triggers
        if(x == -1){
            
            std::cout<<"Invalid input\n";
            continue;
        }

        //check space for occupancy, return to start of loop if triggers
    
            if(boardInHere.board[y][x] != 0){
                std::cout<<"Space already occupied\n";

                goto LOOP;
            }
        
        
        //going right
        if(dir==114){
            //check if ship is on the board
            if(x + shipLength-1 > 9){
                std::cout<<"Ship out of bounds\n";
                goto LOOP;
            }

            if(y>0){//checking space above when not at top row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y-1][x+i] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
                }
            }
            
            if(y<9){//checking space below when not at bottom row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+1][x+i] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
                }
            }

            if(x + shipLength < 10){ //checking space right of ship supposed place
                if(boardInHere.board[y][x+shipLength] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
            }


            for(int i = 0;i < shipLength; i++){// Checking ship supposed location for collision
                if(boardInHere.board[y][x+i] != 0){
                    std::cout<<"Space already occupied\n";
                    goto LOOP;
                }
            }
            
            
            
            
            
            //going down, checking spaces occupancy
        }else if(dir==100){
            
            
            if(y + shipLength-1 > 9){
                std::cout<<"Ship out of bounds\n";
                goto LOOP;
            }

            if(x>0){//checking space left when not at left row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+i][x-1] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
                }
            }
            
            if(x<9){//checking space right when not at right row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+i][x+1] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
                }
            }

            if(y + shipLength < 10){ //checking space below of ship supposed place
                if(boardInHere.board[y+shipLength][x] > 0){
                        std::cout<<"Space already occupied\n";
                        goto LOOP;
                    }
            }
            //check ship length for collisions
            for(int i = 0;i < shipLength; i++){
                if(boardInHere.board[y+i][x] != 0){
                    std::cout<<"Space already occupied\n";
                    goto LOOP;
                }
            }
            
        }


        //Going right, assignments on board
        if(dir==114){

            
            // Creation of buffer zones around ships
            if(y == 0){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+1][x+i] = -1;
                }
            }else if(y == 9){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y-1][x+i] = -1;
                }
            }else{
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y-1][x+i] = -1;
                boardInHere.board[y+1][x+i] = -1;
                }
            }
            if(x >= 1){
                boardInHere.board[y][x-1] = -1;
            }
            if(x+shipLength < 10){
                boardInHere.board[y][x+shipLength] = -1;
            }
            
            //Assign ship id on array
            for(int i = 0;i < shipLength; i++){
                boardInHere.board[y][x+i] = shipsPlaced;
            }
        }else if(dir==100){ // going DOWN from origin, assignments on board

            // Creation of buffer zone around ship
            if(x == 0){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x+1] = -1;
                }
            }else if(x ==9){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x-1] = -1;
                }
            }else{
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x-1] = -1;
                boardInHere.board[y+i][x+1] = -1;
                }
            }
            if(y >= 1){
                boardInHere.board[y-1][x] = -1;
            } 
            if(y + shipLength < 10){
                boardInHere.board[y+shipLength][x] = -1;
            }

            for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x] = shipsPlaced;
            }
        }
        





        shipsPlaced++;
    }
    while(shipsPlaced < 6);
    //clear out buffer symbols from board
    for(int i = 0; i<10; i++){
        for(int j = 0; j<10; j++){
            if(boardInHere.board[i][j]==-1){
                boardInHere.board[i][j]=0;
            }
            
        }
    }

    drawBoard(boardInHere.board, 0);

    return boardInHere;

}

struct boardWrap  placementRandom(){
    struct boardWrap boardInHere;
    for(int i = 0; i<10; i++){
        for(int j = 0; j<10; j++){
            boardInHere.board[i][j]=0;
        }
    }
    srand (time(NULL));//random seed initialization

    int shipsPlaced = 1;


    LOOP:do{


        int shipLength;
        if(shipsPlaced == 1){
            shipLength = 5;
        }else if(shipsPlaced == 2){
            shipLength = 4;
        }else if(shipsPlaced == 4 || shipsPlaced == 3){
            shipLength = 3;
        }else if(shipsPlaced == 5){
            shipLength = 2;
        }


        int x = rand()%10;
        int y = rand()%10;


        //random direction
        int dir = rand()%2;
        if(dir == 1){
            dir = 100;
        }else if(dir == 0){
            dir = 114;
        }


        //check space for occupancy, return to start of loop if triggers
    
            if(boardInHere.board[y][x] != 0){


                goto LOOP;
            }
        
        
        //going right
        if(dir==114){
            //check if ship is on the board
            if(x + shipLength-1 > 9){

                goto LOOP;
            }

            if(y>0){//checking space above when not at top row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y-1][x+i] > 0){

                        goto LOOP;
                    }
                }
            }
            
            if(y<9){//checking space below when not at bottom row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+1][x+i] > 0){

                        goto LOOP;
                    }
                }
            }

            if(x + shipLength < 10){ //checking space right of ship supposed place
                if(boardInHere.board[y][x+shipLength] > 0){

                        goto LOOP;
                    }
            }


            for(int i = 0;i < shipLength; i++){// Checking ship supposed location for collision
                if(boardInHere.board[y][x+i] != 0){

                    goto LOOP;
                }
            }
            
            
            
            
            
            //going down, checking spaces
        }else if(dir==100){
            
            
            if(y + shipLength-1 > 9){

                goto LOOP;
            }

            if(x>0){//checking space left when not at left row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+i][x-1] > 0){

                        goto LOOP;
                    }
                }
            }
            
            if(x<9){//checking space right when not at right row
                for(int i = 0;i < shipLength; i++){
                    if(boardInHere.board[y+i][x+1] > 0){

                        goto LOOP;
                    }
                }
            }

            if(y + shipLength < 10){ //checking space below of ship supposed place
                if(boardInHere.board[y+shipLength][x] > 0){

                        goto LOOP;
                    }
            }
            //check ship length for collisions
            for(int i = 0;i < shipLength; i++){
                if(boardInHere.board[y+i][x] != 0){

                    goto LOOP;
                }
            }
            
        }


        //Going right, assignments on board
        if(dir==114){

            
            // Creation of buffer zones around ships
            if(y == 0){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+1][x+i] = -1;
                }
            }else if(y == 9){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y-1][x+i] = -1;
                }
            }else{
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y-1][x+i] = -1;
                boardInHere.board[y+1][x+i] = -1;
                }
            }
            if(x >= 1){
                boardInHere.board[y][x-1] = -1;
            }
            if(x+shipLength < 10){
                boardInHere.board[y][x+shipLength] = -1;
            }
            
            //Assign ship id on array
            for(int i = 0;i < shipLength; i++){
                boardInHere.board[y][x+i] = shipsPlaced;
            }
        }else if(dir==100){ // going DOWN from origin, assignments on board

            // Creation of buffer zone around ship
            if(x == 0){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x+1] = -1;
                }
            }else if(x ==9){
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x-1] = -1;
                }
            }else{
                for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x-1] = -1;
                boardInHere.board[y+i][x+1] = -1;
                }
            }
            if(y >= 1){
                boardInHere.board[y-1][x] = -1;
            } 
            if(y + shipLength < 10){
                boardInHere.board[y+shipLength][x] = -1;
            }

            for(int i = 0;i < shipLength; i++){
                boardInHere.board[y+i][x] = shipsPlaced;
            }
        }
        





        shipsPlaced++;
    }
    while(shipsPlaced < 6);

    //clear out buffer symbols from board
    for(int i = 0; i<10; i++){
        for(int j = 0; j<10; j++){
            if(boardInHere.board[i][j]==-1){
                boardInHere.board[i][j]=0;
            }
            
        }
    }

    //drawBoard(boardInHere.board,0);

    return boardInHere;   
}

//Re-formats the input for manual placements of ships and controls the validity of input
struct coordWrap inputToCoords(std::string input){
    struct coordWrap shipcoords;
    shipcoords.coords[0] = (int)input[0] - 97;
    shipcoords.coords[1] = (int)input[1] - 48;
    shipcoords.coords[2] = (int)input[2];


    if(shipcoords.coords[0] < 0 || shipcoords.coords[0] > 9){
        shipcoords.coords[0] = -1;
        return shipcoords;
    }else if(shipcoords.coords[1] < 0 || shipcoords.coords[1] > 9){
        shipcoords.coords[0] = -1;
        return shipcoords;
    }else if(shipcoords.coords[2] == 100  || shipcoords.coords[2] == 114){
        return shipcoords;
    }

    // d = 100, u = 117, l = 108, r = 114
    shipcoords.coords[0] = -1;
    return shipcoords;
    
}

//Retrieves the coordinates that the user wants to hit.
struct attack Attack(){
    std::string input;
    struct attack thisattack;
    std::cout<<"Input attack coordinates: \n";
    std::cin>>input;
    thisattack.coords[0] = (int)input[0] - 97;
    thisattack.coords[1] = (int)input[1] - 48;
    if(thisattack.coords[0] < 0 || thisattack.coords[0] > 9){
        thisattack.coords[0] = -1;
        return thisattack;
    }else if(thisattack.coords[1] < 0 || thisattack.coords[1] > 9){
        thisattack.coords[0] = -1;
        return thisattack;
    }
    return thisattack;
}

//Resets the variable that are used for AI homing algorithm
void AI_reset(){
    hit_horizontal = false;
    hit_vertical = false;
    wrong_directionx = false;
    wrong_directiony = 0;
    found_ship = false;
}

void calculateNextMove(){

    //If no ship has been found, the AI will continue aiming in a diagonal fashion
    if(!found_ship){
        calculate_diagonal();//Calculates new coordinates to attack
        for (size_t i = 0; i < 10; i++)
        {
            //The AI will not attack positions that already has been hit or missed.
            if (player_board.board[next_vertical][next_horizontal] == hit || player_board.board[next_vertical][next_horizontal] == miss)
            {
                calculate_diagonal();
            }else{
                break;
            }
        }
    }
    //When the AI hits a ship it should keep looking for that ship until it's destroyed
    else{
        calculate_homing();
    }
}

//This function get called when AI should continue aiming in a diagonal
void calculate_diagonal(){
    last_vertical = AI_diagonal_y;
    last_horizontal = AI_diagonal_x;
    next_horizontal = AI_diagonal_x;
    next_vertical = AI_diagonal_y;

    //If-statements check whether the diagonals are to an end and the AI should start from a new diagonal
    if (last_vertical != board_limit && last_horizontal != board_limit)//Coordinates not at edge of board
    {
        next_horizontal++;
        next_vertical++;
    }else if (last_vertical == board_limit && last_horizontal == board_limit)//Both x and y coordinates at edge of board
    {
        diagonals_searched++;
        next_vertical = board_start;
        next_horizontal = diagonals_searched*2;
    }else if (last_horizontal == board_limit)//x coordinate at edge of board
    {
        diagonals_searched++;
        if (diagonals_searched == 5)
        {
            diagonals_searched = 1;
            next_vertical = diagonals_searched*2;
            next_horizontal = board_start;
        }else{
            next_horizontal = diagonals_searched*2;
            next_vertical = board_start;
        }
    }else if(last_vertical == board_limit){//y coordinate at edge of board
        diagonals_searched++;
        
        next_vertical = diagonals_searched*2;
        next_horizontal = board_start;
    }
    last_horizontal = next_horizontal;
    last_vertical = next_vertical;
    AI_diagonal_y = last_vertical;
    AI_diagonal_x = last_horizontal;
}

void calculate_homing(){

    //Check if the AI should shoot upwards
    if (last_vertical != board_start && hit_horizontal != 1 && hit_horizontal != 2 && player_board.board[last_vertical-1][last_horizontal] != miss && wrong_directiony != 1 && wrong_directiony != 2)
    {
        next_vertical = last_vertical - 1;
        //If the next shot will hit then the AI should look from there next turn
        if (player_board.board[next_vertical][last_horizontal] > empty  && player_board.board[next_vertical][last_horizontal] < 6)
        {
            last_vertical = next_vertical;
            hit_vertical = 1; //Marks the ship as vertical
        }else{
            wrong_directiony = 1;
        }
        next_horizontal = last_horizontal;

    //Check if the AI should shoot to the right
    }else if(last_horizontal != board_limit && hit_vertical != 1 && hit_vertical != 2 && player_board.board[last_vertical][last_horizontal+1] != miss && !wrong_directionx)
    {
        next_horizontal = last_horizontal + 1;
        //If the next shot will hit then the AI should look from there next turn
        if (player_board.board[last_vertical][next_horizontal] > empty && player_board.board[last_vertical][next_horizontal] < 6)
        {
            last_horizontal = next_horizontal;
            hit_horizontal = true;//Marks the ship as horizontal
        }else{
            wrong_directionx = true;
        }
        next_vertical = last_vertical;

    //Check if the AI should shoot downwards
    }else if (wrong_directiony != 2 && hit_horizontal != 1 && hit_horizontal != 2 && player_board.board[last_vertical+1][last_horizontal] != miss)
    {
        //Readjusting the aim when the ship has been hit upwards earlier
        if (hit_vertical == 1 && wrong_directiony == 1)
        {
            for (size_t i = 0; i < 5; i++)
            {
                if (player_board.board[last_vertical+1][last_horizontal] == hit)
                {
                    last_vertical++;
                }else{
                    next_vertical = last_vertical + 1;
                    break;
                }
            }
        }else{
            next_vertical = last_vertical + 1;
        }
        if (player_board.board[next_vertical][last_horizontal] != empty)
        {
            last_vertical = next_vertical;
            hit_vertical = 2;
        }else{
            wrong_directiony = 2;
        }
        next_horizontal = last_horizontal;

    //Shoot to the left if none of the other directions are applicable
    }else
    {
        //Readjusting the aim when the ship has been hit to the right earlier
        if (hit_horizontal && (wrong_directionx || last_horizontal == board_limit))
        {
            for (size_t i = 0; i < 5; i++)
            {
                if (player_board.board[last_vertical][last_horizontal-1] == hit)
                {
                    last_horizontal--;
                }else{
                    next_horizontal = last_horizontal - 1;
                    break;
                }
            }
        }else{
            next_horizontal = last_horizontal - 1;
        }
        next_vertical = last_vertical;
        hit_horizontal = 2;
        last_horizontal--;
        next_horizontal = last_horizontal;
    }
}

void game_statistics()
{
    std::cout << "Game length: " << player_turns + cpu_turns << " turns." << std::endl;
    std::cout << "Player took: " << player_turns << " turns and CPU took " << cpu_turns << std::endl;
    std::cout << "Player hit " << player_hits << " times and destroyed " << player_destroyed << " ships dealing " << player_hits << " hp damage in total" << std::endl;
    std::cout << "CPU hit " << cpu_hits << " times and destroyed " << cpu_destroyed << " ships dealing " << cpu_hits << " hp damage in total" << std::endl;

}