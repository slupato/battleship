# Battleship

This project is a game of battleships. In this game player plays against the AI. Player will have the option to manually assign the ships on the board, or use randomly generated board. The AI will always use randomly generated board. Players take turns shooting a shot at coordinates a-j and 0-9. The gameplay is visually represented in terminal, and user input is made with text input.

## Usage

Use of makefile:
Make creates new folder and saves compiled game into it.

**Compile**
```
make battleship
```

***Compile and run***
```
make
```

***Run the game***
```
make run
```

***Delete files and folders***
```
make clean
```

## Description and Game instructions
Both players in the game have a fleet consisting of 5 ships of varying lengths:
Cruiser (5 squares)
Battleship (4 squares)
Destroyer (3 squares)
Submarine (3 squares)
Patrol boat (2 squares)

Ships are marked on the board by their initial capital letter

When game starts, player selects 
1 for randomly assigned game board
2 for player to manually input ship positions


In manual placement mode, player sets a ship on the board with a text command, for example, a5r, consisting of origin coordinates, and direction in which the ship extends from the origin.

first letter a-j selects the column for the origin coordinate
2nd number selects the row for the origin coordinate
3rd letter d for down, or r for right sets the ship orientation

Ships may not go over the board limits, and may not touch each other.
On the placement board, the . marks the buffer zone around the ship where another ship may not reside.

When all 5 ships are properly set, the game begins.

Player is asked input coordinates for the shot. When player inputs valid coordinates, they are checked if the location contains an enemy ship, or is a miss. Then turn is passed over to AI which does the same against player. If the hit sinks an enemy ship, it is announced as well. A ship is sunk when each square it occupies are shot. The game ends when either the player or ai runs out of ships.

When the game is over a set of statistics is presented to the player, and the program ends.


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Authors and acknowledgment
This project was made by:
Hannes Ringblom
Juho Kangas
Tuomas Uusi-Luomalahti

## Contribution by persons
Juho Kangas: Filling game boards manually and randomly, Visual representation of the board and the associated game modes, taking player input to a coordinate, readme content

Tuomas Uusi-Luomalahti: Generating ships, Ship damage, destroy and winning condition functions, Statistic counters, Makefile, Readme content

Hannes Ringblom: Developed AI basic aiming functions and homing algorithm, attack functions for player and AI, gameloop functionality.

