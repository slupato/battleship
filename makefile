MYDIR=./build

all: battleship run

battleship:
	[ -d $(MYDIR) ] || mkdir -p $(MYDIR)
	g++ src/battleship.cpp -o build/battleship

run:
	./build/battleship


clean:
	rm -r build